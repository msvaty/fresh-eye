app.constant("APP_CONFIG", {
    //Basic config
    debugMode: true,
    storagePrefix: "test_",
    defaultState: "/menu/home",

    // API config
    apiKey: "4aa883f95999ec813b8bfaf319f3972b",
    apiUrl: "fresh-eye.cz/wp-json/wp/v2/",
    apiEndpoints: {
        moviesPopular: "video",
         posts: "posts"
    },
    
    getApiUrl: function (endpoint){
        return "http://" + this.apiUrl + this.apiEndpoints[endpoint]//+"&page="+this.page;
    },
    getApiUrlMovies: function (id){
        return "http://" + this.apiUrl + "video/"+id;
    },
    getApiUrlArticle: function (id){
        return "http://" + this.apiUrl + "posts/"+id;
    }
});
