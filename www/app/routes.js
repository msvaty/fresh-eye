app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider, $resourceProvider, APP_CONFIG) {
    $httpProvider.interceptors.push('authInterceptorService');
    $ionicConfigProvider.scrolling.jsScrolling(false);

    $stateProvider


         .state('movies', {
                url: '/movies',
                cache: false,
                templateUrl: "views/movies/movies-list.html",
                controller: 'MoviesListCtrl'
            })
            .state('movies-detail', {
                url: '/movies/:id',
                cache: false,
                templateUrl: "views/movies/movies-detail.html",
                controller: 'MoviesDetailCtrl'
            })

            .state('article', {
                url: '/articles',
                cache: false,
                templateUrl: "views/articles/article-list.html",
                controller: 'ArticleListCtrl'
            })
            .state('article-detail', {
                url: 'views/articles/:id',
                cache: false,
                templateUrl: "views/articles/article-detail.html",
                controller: 'ArticleDetailCtrl'
            })
        // states end*/
        ;

    $urlRouterProvider.otherwise("/movies");

});
